﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DrawLine : MonoBehaviour {
	public enum LineType{
		Line,
		BGLine
	};

	public GameObject rectBox;
	public GameObject stream;
	public Image imageLine;
	public Image imageBGLine;
	RectTransform imageLineRT;
	RectTransform imageBGLineRT;

	Vector2 scrSize;
	float scaleX;
	float widthLine = 3f;
	float widthBGLine = 1f;

	Vector3 BGLeftHigh, BGLeftMiddle, BGLeftLow;
	Vector3 BGRightHigh, BGRightMiddle, BGRightLow;
	Vector3 pointA, pointB;

	void Awake(){
		imageLineRT = imageLine.GetComponent<RectTransform>();
		imageBGLineRT = imageBGLine.GetComponent<RectTransform>();
	}

	// Use this for initialization
	void Start () {
		scrSize = new Vector2((float)Screen.width, (float)Screen.height);
		scaleX = scrSize.x / 10f;

		BGLeftHigh = new Vector3(0, scrSize.y * 0.9f, 0);
		BGLeftMiddle = new Vector3(0, scrSize.y * 0.75f, 0);
		BGLeftLow = new Vector3(0, scrSize.y * 0.6f, 0);
		BGRightHigh = new Vector3(scrSize.x, scrSize.y * 0.9f, 0);
		BGRightMiddle = new Vector3(scrSize.x, scrSize.y * 0.75f, 0);
		BGRightLow = new Vector3(scrSize.x, scrSize.y * 0.6f, 0);

		Draw(LineType.BGLine, BGLeftHigh, BGRightHigh);		
		Draw(LineType.BGLine, BGLeftMiddle, BGRightMiddle);
		Draw(LineType.BGLine, BGLeftLow, BGRightLow);

		pointA = new Vector3(scaleX, scrSize.y * (0.5f + 0.5f * 0.8f), 0);
		pointB = new Vector3(scaleX * 2f, scrSize.y * (0.5f + 0.5f * 0.4f), 0);

		Draw(LineType.Line, pointA, pointB);
	}

	void Draw(LineType lt, Vector3 A, Vector3 B){
		float width = widthLine;
		GameObject go;
		RectTransform rt = null;
		switch(lt){
		case LineType.BGLine:
			width = widthBGLine;
			go = Instantiate(GameObject.Find("ImageBGLine"));
			go.transform.SetParent(rectBox.transform);
			rt = go.GetComponent<RectTransform>();
			break;

		case LineType.Line:
			go = Instantiate(GameObject.Find("ImageLine"));
			go.transform.SetParent(stream.transform);
			rt = go.GetComponent<RectTransform>();
			break;
		}
		
		Vector3 differenceVector = B - A;			
		rt.sizeDelta = new Vector2( differenceVector.magnitude, width);
		rt.pivot = new Vector2(0, 0.5f);
		rt.position = A;
		float angle = Mathf.Atan2(differenceVector.y, differenceVector.x) * Mathf.Rad2Deg;
		rt.rotation = Quaternion.Euler(0,0, angle);
	}


}
